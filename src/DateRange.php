<?php

/*
 * This file is part of the P13/DateTime package.
 *
 * (c) Wagner Sicca <wssicca@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace p13\datetime;

use \DatePeriod;

/**
 * Classe que representa um range de datas
 * 
 *
 * @author Wagner Sicca <wssicca@gmail.com>
 * @namespace p13\datetime
 * @package p13\datetime
 */
class DateRange
{

    /**
     * Data final da semana (domingo)
     * @var DateTime
     */
    public $endDate;

    /**
     * Data inicial da semana (segunda)
     * @var DateTime
     */
    public $startDate;

    /**
     * 
     * @param \p13\datetime\DateTime $startDate
     * @param \p13\datetime\DateTime $endDate
     */
    public function __construct(DateTime $startDate, DateTime $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * Retorna uma representação textual da semana
     * @return string
     */
    public function __toString()
    {
        return sprintf(
                "%s a %s", $this->startDate->format('d/m/Y'), $this->endDate->format('d/m/Y')
        );
    }

    /**
     * Verifica se determinada data está contido no intervalo de data do objeto
     * @param \p13\datetime\DateTime $data
     * @return boolean
     */
    public function contains(DateTime $data)
    {
        return $data >= $this->startDate && $data <= $this->endDate;
    }

    /**
     * Retorna um objeto DatePeriod relacionado ao intervalo de datas
     * @return \DatePeriod
     */
    public function getDatePeriod()
    {
        $intervaloDiario = DateInterval::createFromDateString('1 day');
        $dataFim = clone $this->endDate;
        $dataFim->add($intervaloDiario);
        return new DatePeriod($this->startDate, $intervaloDiario, $dataFim);
    }

    /**
     * Verifica se há alguma coincidência de datas (qualquer que seja) entre
     * o intervalo informado e este objeto
     * @param \p13\datetime\DateRange $dateRange
     * @return boolean
     */
    public function hasIntersection(DateRange $dateRange)
    {
        return ($dateRange->startDate >= $this->startDate && $dateRange->startDate <= $this->endDate) ||
                ($dateRange->endDate >= $this->startDate && $dateRange->endDate <= $this->endDate) ||
                ($dateRange->startDate < $this->startDate && $dateRange->endDate > $this->endDate);
    }

}
